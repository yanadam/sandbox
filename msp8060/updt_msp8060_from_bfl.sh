img_dir=${1:-ftp://10.30.2.54/temp/RD01068_MSP8060/LAN_eeprom}
#img_dir must contain these files:
msp8060_x552_nvm_img=BDXDE_KR_Backplane_NO_MNG_FEC_Request_1.13_80000791.bin
msp8060_x710_nvm_img=XL710_X710_6p01_KR_KX_NCSI_CFGID0p0_Dual.bin

tools_dir=${2:-ftp://10.30.2.54/temp/Intel_LAD_LAN_tools}
#tools_dir must contain these files:
eeupdate="eeupdate64e_v5.30.25.06"
bootutil="bootutil64e_v1.6.57.0"

boardsernum=$(dmidecode | grep "Serial Number: 901" | head -n1 | cut -d" " -f3)
cpu=$(ipmitool sdr list mcloc | cut -d " " -f 12)

tmp_dir=$(mktemp -d)
pushd .
cd $tmp_dir
log_dir=${3:-~/}


echo "Getting installation files from $img_dir and $tools_dir"
STATUS="1"
wget $img_dir/$msp8060_x552_nvm_img && \
wget $img_dir/$msp8060_x710_nvm_img && \
wget $tools_dir/$eeupdate && \
wget $tools_dir/$bootutil  && STATUS=0
if [ $STATUS -ne 0 ] ; then \
        echo "Error getting files, quitting before making changes"
        exit 1
fi
chmod +x $eeupdate $bootutil 
./$eeupdate /nic=1 /d $msp8060_x552_nvm_img 2>&1 | tee $log_dir/${boardsernum}_${cpu}.log
./$eeupdate  /nic=3 /d $msp8060_x710_nvm_img 2>&1 | tee -a $log_dir/${boardsernum}_${cpu}.log
./$bootutil -all -fd 2>&1 | tee -a $log_dir/${boardsernum}_${cpu}.log

popd
