#!/bin/sh

dd if=/dev/zero of=/dev/mmcblk0 count=20480

echo -n "Creeating \"boot\" partition ..."
parted -s /dev/mmcblk0 'mklabel gpt'
parted -s /dev/mmcblk0 'mkpart boot ext4 2048s 768MB'
sync
echo -n " Formatting ..."
mkfs.ext4 -F -O ^extents,^64bit /dev/mmcblk0p1
sync
echo "Done"
echo -n "Creeating \"boot\" partition ..."
parted -s /dev/mmcblk0 'mkpart PicOS ext4 768MB 3208MB'
sync
echo -n " Formatting ..."
mkfs.ext4 -F -O ^extents,^64bit /dev/mmcblk0p2
sync
echo -n "Creeating \"boot\" partition ..."
parted -s /dev/mmcblk0 'mkpart PICOS-BAK ext4 -768MB 100%'
echo "Done"
sync
echo -n " Formatting ..."
mkfs.ext4 -F /dev/mmcblk0p3
echo "Done"
sync

parted -l /dev/mmcblk0

for i in 1 2; do mkdir /mnt/usd_p${i}; mount /dev/mmcblk0p${i} /mnt/usd_p${i}; done

UUID=$(cat /proc/sys/kernel/random/uuid) #Might want to check how KSB generates this...
mkdir -p /mnt/usd_p1/bootorder/ /mnt/usd_p1/images/${UUID} /mnt/usd_p1/images/recovery

wget ftp://10.30.2.54/temp/4303/picos_install/msh8920-picos-latest.tar.gz
wget ftp://10.30.2.54/temp/4303/picos_install/bootloader -P /mnt/usd_p1/images/${UUID}
wget ftp://10.30.2.54/temp/4303/picos_install/recovery/kontron-t4303.dtb -O /mnt/usd_p1/images/recovery/dtb
wget ftp://10.30.2.54/temp/4303/picos_install/recovery/kernel -P /mnt/usd_p1/images/recovery/
wget ftp://10.30.2.54/temp/4303/picos_install/recovery/rootfs -O /mnt/usd_p1/images/recovery/ramdisk

tar -zx -C /mnt/usd_p2 -f ./msh8920-picos-latest.tar.gz
sync

ln -s ../images/recovery/ /mnt/usd_p1/bootorder/r
ln -s ../images/${UUID} /mnt/usd_p1/bootorder/1
sync

umount /mnt/usd_p*

#This way of setting U-Boot environment does not work at the moment, variables will have to be entered in U-Boot manually after reboot
#fw_setenv watchdog_picos 0
#fw_setenv bootsource picos
#fw_setenv setbootargs_picos 'setenv bootargs console=ttyS0,${baudrate} root=/dev/mmcblk0p2 init=/sbin/init'
#fw_setenv bootcmd_picos 'run setbootargs_picos && watchdog ${watchdog_picos} && load mmc 0:2 ${kerneladdr} boot/uImage && load mmc 0:2 ${fdtaddr} boot/kontron_t4303.dtb && bootm ${kerneladdr} - ${fdtaddr}'

#fw_printenv

echo "Clearing environment"
dd if=/dev/zero of=/dev/mtd3
echo "You are now ready to reboot"
echo "Please note that you will have to stop U-Boot and enter these commands to add the variables that will allow booting off the installed PicOS"
echo "==========================================================================================================================================="
echo "setenv watchdog_picos stop"
echo "setenv bootsource picos"
echo "setenv setbootargs_picos 'setenv bootargs console=ttyS0,\${baudrate} root=/dev/mmcblk0p2 init=/sbin/init'"
echo "setenv bootcmd_picos 'run setbootargs_picos && watchdog \${watchdog_picos} && load mmc 0:2 \${kerneladdr} boot/uImage && load mmc 0:2 \${fdtaddr} boot/kontron_t4303.dtb && bootm \${kerneladdr} - \${fdtaddr}'"
echo "saveenv"
echo "reset"
