The files here can be used to start the bcm.shell (a.k.a. Broadcom shell) with a configuration closer to final MSH8920A (with BCM56760 switch).

Upload all these files to the /etc/bcm directory and from that directory start with:
". start_bcm.sh"
