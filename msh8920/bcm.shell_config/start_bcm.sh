#!/bin/sh

BCM_CONF=/etc/bcm

function fpga {
    if [ $# -eq 1 ]; then
	devmem $(( 0xfff000000 + $1 )) 8
    elif [ $# -eq 2 ]; then
	devmem $(( 0xfff000000 + $1 )) 8 $2
    else
	echo "usage: fpga addr [data]"
	exit 1
    fi
}

#echo "** Watchdog disabled..."
#fpga 0x50 0

# Config files selection
TJ=$(( $(fpga 0xdf) & 0x01 ))
GA=$(( $(fpga 0x91) & 0x01 ))
CA=$(( $(fpga 0xe2) ))

if [ $TJ == 1 ]; then
    echo "** T4303 in test JIG"
    BCM=T4303_h1.bcm
    SOC=T4303_jig.soc
    cp $BCM_CONF/T4303_rc.soc $BCM_CONF/$SOC
    echo "port \$base,\$fabric,\$xsw en=0" >> $BCM_CONF/$SOC
else
    if [ $GA == 0 ]; then
	echo "** T4303 in Hub 1 slot"
	BCM=T4303_h1.bcm
	SOC=T4303_rc.soc
	BCMGA=config-h1.bcm
    else
	echo "** T4303 in Hub 2 slot"
	BCM=T4303_h2.bcm
	SOC=T4303_rc.soc
	BCMGA=config-h2.bcm
    fi
fi

# create a config.bcm for the given GA
# strip comments (EOL comments not supported)
# and blank lines
cat $BCM_CONF/$BCM | sed -e 's/\s*#.*$//g' -e '/^\s*$/d' > $BCM_CONF/$BCMGA
# copy the rc.soc
cp $BCM_CONF/$SOC $BCM_CONF/rc-local.soc

echo "** Starting bcm.user from $(pwd) with $BCM_CONF/$BCM"
echo "** Setting front RJ45 mux to switch"
fpga 0xe0 0x01
echo "** Enabling QSFP"
fpga 0x80 0x0
fpga 0x81 0x0
fpga 0x82 0x0
echo -e "\n"

# udhcpc -i eth1 -b

( cd $BCM_CONF && exec /opt/bcmsdk/bin/bcm.user )

echo "** Setting front RJ45 mux to UC"
fpga 0xe0 0x00
echo "** Disabling QSFP"
fpga 0x80 0x20
fpga 0x81 0x20
fpga 0x82 0x20
